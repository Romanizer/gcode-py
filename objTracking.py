import cv2
import serial
import time
import imutils

ser = serial.Serial('COM20', 115200)
ser.write(b'\n')  # wakes up the board
time.sleep(2)
ser.write(b'G28\r\n')
ser.flush()
time.sleep(1)


# detect only green obj
greenLower = (29, 86, 6)
greenUpper = (64, 255, 255)

camera = cv2.VideoCapture(1)

while True:

    _, frame = camera.read()
    (w, h, c) = frame.shape

    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, greenLower, greenUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cnt, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2:]


    for c in range(len(cnt)):

        x, y, w, h = cv2.boundingRect(cnt[c])
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        x2 = int(x/5)
        y2 = int(y/5)
        coords = 'X' + str(x2) + ' ' + 'Y' + str(y2)
        print('coords: ' + coords)
        test = 'G1 ' + coords
        ser.write((test + ' F900\r\n').encode())
        ser.flush()
        time.sleep(1)
        cv2.circle(frame, (x2, y2), 4, (0, 255, 0), -1)

    cv2.imshow("Frame", frame)
    cv2.imshow("Mask", mask)


    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

camera.release()
cv2.destroyAllWindows()
ser.close()