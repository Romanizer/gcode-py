# GCode Sending Library

Python Library and sample code: Sending GCode to Marlin Printers etc.

## Overview

* `gcodesender.py`: Library to include in python script
* `sample_route.py`: Sample code instructing the printer to follow a pre-defined sample route
* `object_detect.py`: Script detecting green objects and instructing the printer to drive to the position of the object

## How the Code works

The Library was written in a way to allow the printer to drive as fast as possible, without flooding the serial connection with gcode.
To achive this, we calculate the time (in seconds) it takes for the printer to move from point A to point B. 
Before we issue another gcode command, we wait for this time to elapse.

## Working with the code

Firstly: `from gcodesender import *`

Before the main loop, initialise the GCodeSender with the Serial Port: `gcodesender = GCodeSender('COM1')`

In the mainloop, we want to check if the time has elapsed, i.e. we are allowed to send another gcode command.

To do this we compare a variable to systemtime. This variable `nextCommandTime` is initialized as `0` before the main loop.

```python
if nextCommandTime < time.time()
    timeToWait = gcodesender.send(x,y,z,feedrate)
    nextCommandTime = time.time() + timeToWait
```

After exiting the main loop, we can end the connection and close the serial port using: `gcodesender.cleanup()`

For a more in-depth look, see the two sample scripts: `sample_route.py` and `object_detect.py`

This is the "Blueprint" for using this Library:
```python
from gcodesender import *
import time

nextCommandTime = 0

gcodesender = GCodeSender('COM1')

while True:

    #busy work

    if nextCommandTime < time.time():
        timeToWait = gcodesender.send(x,y,z,feedrate)
        nextCommandTime = time.time() + timeToWait

    #exit condition

gcodesender.cleanup()
```
