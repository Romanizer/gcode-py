import serial
import time
import math
from gcodesender import *
from array import *

# init params for operation
feedrate = 4200 # mm/min   #  4300 mm/min is too fast, and the robot arm cant keep up
z_pos = 170    # Z height for this 2D route
nextCommandTime = 0

gcsender = GCodeSender('COM7')

# Sample Route [x,y]
route = [[100, 100], [100, -100], [-100, -100], [-100, 100]]

index = 0

while True:
    
    # simulate busy work:
    time.sleep(1/60)
    
    if nextCommandTime < time.time():
        
        x, y = route[index]
        index = index + 1
        # send G-Code and capture time needed for operation
        timeToWait = gcsender.send(x, y, z_pos, feedrate)
        # calculate next time a command can be issued
        nextCommandTime = time.time() + timeToWait
        print('timetowait: ' + str(timeToWait) + '\n')
        
    if index >= len(route):
        #break   # uncomment for single loop
        index = 0  # uncomment for endless loop

print("code completed")
gcsender.cleanup()
