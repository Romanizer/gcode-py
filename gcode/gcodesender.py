import serial
import time
import math

class GCodeSender:
    # Max Machine Parameters
    max_x = 100  # 200x200 print vol -> 100 in each direction
    min_x = -100
    max_y = 100
    min_y = -100
    max_z = 245  # also known as home_z
    min_z = 0

    # Homing Parameters
    home_x = 0
    home_y = 0
    home_z = max_z
    home_z_after = 210  # after homing when initialized, Z drives to this height.

    def __init__(self, port):
        self.ser = serial.Serial(port, 115200)
        self.ser.write(b'\n')  # wakes up the board
        self.ser.flush()  # always flush it after a command
        time.sleep(5)  # wait for waking it up
        self.ser.write(b'G28 \r\n')  # firstly, send home position
        self.ser.write(b'G90 \r\n')  # absolute movements
        self.ser.write(b'G1 Z' + str(self.home_z_after).encode() + b' \r\n')  # little drive down, because using rotary delta setup
        self.ser.flush()
        self.last_x = self.home_x  # set last pos to home pos after homing
        self.last_y = self.home_y
        self.last_z = 100
        time.sleep(5)

    def send(self, x, y, z, f):
        if x > self.max_x: return 0 # dont send command if x is outside of printers work area
        if x < self.min_x: return 0
        if y > self.max_y: return 0
        if y < self.min_y: return 0
        if z > self.max_z: return 0
        if z < self.min_z: return 0
        test = 'G1 X' + str(x) + ' Y' + str(y) + ' Z' + str(z) + ' F' + str(f)
        print('Issuing Command: ' + test)
        self.ser.write((test + '\r\n').encode())
        self.ser.flush()
        rel_x = x - self.last_x  # calc relative change in position from last position
        rel_y = y - self.last_y
        rel_z = z - self.last_z
        t = math.sqrt(rel_x * rel_x + rel_y * rel_y + rel_z * rel_z) * (60 / f) * 0.9  # 3D Pythagoras of x,y,z * feedrate in sec/mm * calibration
        self.last_x = x  # update last position to current position
        self.last_y = y
        self.last_z = z
        return t

    def cleanup(self):
        self.ser.close()

    def enablePump(self):
        # turn the pump on (D9 on Rambo PCB)
        # set the pump to 100% power. 255 is full speed (12V)
        self.ser.write(b'M106 S255 \r\n')
        self.ser.flush()

    def disablePump(self):
        # turn off pump completely. 0 is no speed
        self.ser.write(b'M106 S0 \r\n')
        self.ser.flush()
