from gcodesender import *
import time


GCodeSender = GCodeSender("/dev/ttyUSB0")

start = time.time()

GCodeSender.enablePump()

while True:
    time.sleep(1)
    print('pumping water...')
    # pump turns off automatically after 10 sec
    if time.time() - start > 10:
        GCodeSender.disablePump()
        print('stop pumping water...')
        break
