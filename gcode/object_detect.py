import cv2
import serial
import time
import imutils
import math
from gcodesender import *

# detect only green obj
greenLower = (29, 86, 6)
greenUpper = (64, 255, 255)

feedrate = 4200 # mm/min   #  4300 mm/min is too fast, and the robot arm cant keep up
z_pos = 170  
nextCommandTime = 0

camera = cv2.VideoCapture(0)

gcsender = GCodeSender('COM7')

while True:
    ret, frame = camera.read()

    # frame = imutils.resize(frame, width=600, height=300)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, greenLower, greenUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    cnt, hierarchy = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2:]
    if nextCommandTime < time.time():
        for c in range(len(cnt)):
            # determine positions + bounding box
            x, y, w, h = cv2.boundingRect(cnt[c])
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            # center positions
            x2 = int(x + w / 2)
            y2 = int(y + h / 2)

            # send G-Code and capture time needed for operation
            timeToWait = gcsender.send(x2, y2, z_pos, feedrate)
            # calculate next time a command can be issued
            nextCommandTime = time.time() + timeToWait
            print('timetowait: ' + str(timeToWait) + '\n')


            # draw center in bounding box
            cv2.circle(frame, (x2, y2), 4, (0, 255, 0), -1)

    cv2.imshow("Frame", frame)
    cv2.imshow("Mask", mask)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

gcsender.cleanup()
camera.release()
cv2.destroyAllWindows()