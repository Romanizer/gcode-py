# gcode-py

python control of cnc machines and 3d printers for ML/AI applications

## IcePick Delta / Marlin Firmware

The Firmware is located in the `Marlin` directory. Open the `Marlin.ino` file in arduino 1.0.6.
You may need to edit `Configuration.h` when making changes to the robot.

Use these Settings in the Arduino IDE to upload Icepick/Marlin:
* Board: Arduino Mega 2560 or Mega ADK
* Programmer: USBtinyISP
* Baudrate: 115200